-module(rss_handler).
-export([init/3, allowed_methods/2, allow_missing_post/2,
         content_types_accepted/2,
         content_types_provided/2,
         resource_exists/2,
         from_any/2, to_rss/2]).

init({tcp, http}, _Req, _Opts) ->
    {upgrade, protocol, cowboy_rest}.

allowed_methods(Req, Opts) ->
    {[<<"GET">>,<<"HEAD">>,<<"POST">>,<<"OPTIONS">>], Req, Opts}.

allow_missing_post(Req, Opts) ->
    {true, Req, Opts}.

content_types_accepted(Req, Opts) ->
    {[{'*', from_any}],
     Req, Opts}.

content_types_provided(Req, Opts) ->
    {[{<<"application/rss+xml">>, to_rss},
      {<<"application/rdf+xml">>, to_rss},
      {<<"application/xml">>, to_rss},
      {<<"text/rss">>, to_rss},
      {<<"text/xml">>, to_rss},
      {<<"*/*">>, to_rss}],
     Req, Opts}.

resource_exists(Req0, Opts) ->
    {Name, Req1} = cowboy_req:binding(name, Req0),
    {rss_generator:exists(Name), Req1, Opts}.

from_any(Req0, _Opts) ->
    {Opts,Req1} = cowboy_req:qs_vals(Req0),
    Desc = proplists:get_value(<<"description">>, Opts, <<"No description">>),
    Delay = proplists:get_value(<<"interval">>, Opts),
    {Name,Req2} = cowboy_req:binding(name, Req1),
    {<<"POST">>, Req3} = cowboy_req:method(Req2),
    case Delay of
        undefined ->
            rss_generator:start_feed(Name, Name, Desc);
        Delay ->
            rss_generator:start_feed(Name, Name, Desc, 1000*binary_to_integer(Delay))
    end,
    {Path, Req4} = cowboy_req:path(Req3),
    {HostUrl, Req5} = cowboy_req:host_url(Req4),
    {{true, [HostUrl,Path]}, Req5, Opts}.

to_rss(Req0, _Opts) ->
    {Opts,Req1} = cowboy_req:qs_vals(Req0),
    {Name,Req2} = cowboy_req:binding(name, Req1),
    Feed = rss_generator:get_feed(Name),
    {Path, Req3} = cowboy_req:path(Req2),
    {HostUrl, Req4} = cowboy_req:host_url(Req3),
    Url = [HostUrl,Path],
    Body = ["<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"
        "<rss version=\"2.0\">\n"
        "<channel>\n"
        "  <title>", proplists:get_value(title, Feed), "</title>\n"
        "  <description><![CDATA[", proplists:get_value(description, Feed), "]]></description>\n"
        "  <link>", Url, "</link>\n"
        "  <lastBuildDate>", proplists:get_value(pubdate, Feed), "</lastBuildDate>\n"
        "  <pubDate>", proplists:get_value(pubdate, Feed), "</pubDate>\n"
        "  <ttl>", integer_to_list(proplists:get_value(ttl, Feed) div 1000), "</ttl>\n",
        [["  <item>\n"
                "   <title>", proplists:get_value(title, Entry), "</title>\n"
                "   <description>", proplists:get_value(description, Entry), "</description>\n"
                "   <link>", HostUrl, "/posts", proplists:get_value(link, Entry), "</link>\n"
                "   <guid isPermaLink=\"false\">", proplists:get_value(guid, Entry), "</guid>\n"
                "   <pubDate>", proplists:get_value(pubdate, Entry), "</pubDate>\n"
                "  </item>\n"] || Entry <- proplists:get_value(items, Feed)],
        "</channel>\n"
        "</rss>"],
    {Body, Req4, Opts}.

